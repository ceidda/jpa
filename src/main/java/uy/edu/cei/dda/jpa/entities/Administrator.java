package uy.edu.cei.dda.jpa.entities;

import javax.persistence.Entity;

@Entity
public class Administrator extends Person {

	private String role;

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

}
