package uy.edu.cei.dda.jpa;

import java.util.ArrayList;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import uy.edu.cei.dda.jpa.entities.Address;
import uy.edu.cei.dda.jpa.entities.Administrator;
import uy.edu.cei.dda.jpa.entities.Customer;
import uy.edu.cei.dda.jpa.entities.Person;

/**
 * Hello world!
 *
 */
public class App {
	public static void main(String[] args) {
		EntityManagerFactory emf;
		emf = Persistence.createEntityManagerFactory("jpaPU");
		EntityManager em = emf.createEntityManager();
		
		
		em.getTransaction().begin();
		
		Person p = new Person();
		p.setName("jose");
		
		p.setAddresses(new ArrayList<Address>());
		
		Address address = new Address();
		address.setLine1("linea 1");
		p.getAddresses().add(address);
		
		address = new Address();
		address.setLine1("linea 2");
		p.getAddresses().add(address);
		
		//em.persist(address);
		em.persist(p);
		
		p.setName("pepe");
		System.out.println("------- 1");
		
		Administrator admin = new Administrator();
		admin.setRole("administrator");
		em.persist(admin);
		
		em.getTransaction().commit();
		
		em.clear();
		
		System.out.println("------- 2");
	
		Person p1 = em.find(Person.class, p.getId());
		System.out.println(p1.getName()); //jose
		p1.getAddresses().forEach(a -> System.out.println(a.getLine1()));		
		System.out.println("finish");
		
		em.clear();
		
		System.out.println("**** customer ****");
		TypedQuery<Person> query = em.createNamedQuery("personFindByName", Person.class);
		query.setParameter("name", "pepe");
		
		Person p2 = query.getSingleResult();
		p2.setName("jose");
		
		em.close();
		emf.close();
		
		emf = Persistence.createEntityManagerFactory("jpaPU");
		em = emf.createEntityManager();
		
		em.getTransaction().begin();
		p2 = em.merge(p2);
		em.persist(p2);
		em.getTransaction().commit();
		
		em.close();
		emf.close();
	}
}
