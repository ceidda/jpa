package uy.edu.cei.dda.jpa.entities;

import javax.persistence.Entity;

@Entity
public class Customer extends Person {

	private String customerId;

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

}
